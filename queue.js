let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.




function print(){
	return collection;
}
function enqueue(name){
	collection[collection.length] = name;
	return collection;
}
function dequeue(){
	let newCollection=[];
	for(let i=0;i<collection.length;i++){
		if(i!=0){
			newCollection[newCollection.length] = collection[i];
		}
	}
	collection=newCollection;
	return collection;
}

function front(){

	return collection[0];
}
function size(){

	return collection.length;
}
function isEmpty(){
	if(collection.length==0){
	 	return true;
	}else{
		 return false;
	}
}
module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
	
};